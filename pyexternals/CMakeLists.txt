#---General parameters--------------------------------------------------------------------------------------------
set(Python_cmd ${Python_home}/bin/python)
set(PythonFW_cmd ${Python_home}/Library/Frameworks/Python.framework/Versions/Current/bin/python)
set(GenURL http://service-spi.web.cern.ch/service-spi/external/tarFiles)
set(MakeSitePackagesDir ${CMAKE_COMMAND} -E  make_directory <INSTALL_DIR>/lib/python${Python_config_version_twodigit}/site-packages)

#---Addtional deopendencies to matplotlib-----------------
set(matplotlib_extra_deps python_dateutil six tornado backports certifi messaging)

#=================================================================================================================
#---pygraphics----------------------------------------------------------------------------------------------------
# This is a bundle where all the sub-packages will be installed
# First we define all sub-packages and in the end the master-package
#=================================================================================================================
LCGPackage_set_home(pygraphics)

#---pydot------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  pydot
  URL ${GenURL}/pydot-${pydot_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---sip------------------------------------------------------------------------------------------------------
if(LCG_TARGET MATCHES i386-mac)
  set(sip_compile_options -p darwin-g++ CFLAGS=-m32 CXXFLAGS=-m32 CC=gcc CXX=g++ LINK=g++)
elseif(LCG_TARGET MATCHES x86_64-mac)
  set(sip_compile_options --arch=x86_64)
elseif(LCG_TARGET MATCHES slc|ubuntu AND LCG_TARGET MATCHES x86_64)
  set(sip_compile_options -p linux-g++-64)
elseif(LCG_TARGET MATCHES slc|ubuntu AND LCG_TARGET MATCHES i686)
  set(sip_compile_options -p linux-g++-32)
endif()
if (APPLE)
  set (Python_cmd_sip ${PythonFW_cmd})
else()
  set (Python_cmd_sip ${Python_cmd})
endif()
LCGPackage_Add(
  sip
  URL ${GenURL}/sip-${sip_native_version}.tar.gz
  CONFIGURE_COMMAND ${Python_cmd_sip} configure.py -b <INSTALL_DIR>/bin
                                               -d <INSTALL_DIR>/lib/python${Python_config_version_twodigit}/site-packages
                                               -e <INSTALL_DIR>/include/python${Python_config_version_twodigit}
                                               -v ${sip_home}/share/sip ${sip_compile_options}
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---pyqt------------------------------------------------------------------------------------------------------
if(APPLE)
  set(pyqt_platform mac)
  set(Python_cmd_pyqt ${PythonFW_cmd})
else()
  set(pyqt_platform x11)
  set(Python_cmd_pyqt ${Python_cmd})
endif()

# Create qt.conf to locate the installing of Qt. The Qt library still contains hardwired locations and this is
# used by pyqt to locate Qt. 
file(WRITE ${CMAKE_BINARY_DIR}/qt.conf "
[Paths]
Prefix=${Qt_home}
")
file(WRITE ${CMAKE_BINARY_DIR}/qt5.conf "
[Paths]
Prefix=${Qt5_home}
")

LCGPackage_Add(
  pyqt
  URL ${GenURL}/PyQt-${pyqt_platform}-gpl-${pyqt_native_version}.tar.gz
  PATCH_COMMAND      ${CMAKE_COMMAND} -E copy ${CMAKE_BINARY_DIR}/qt.conf qt.conf
  CONFIGURE_COMMAND  ${Python_cmd_pyqt} configure.py -b <INSTALL_DIR>/bin
                                               -d <INSTALL_DIR>/lib/python${Python_config_version_twodigit}/site-packages
                                               -l ${Python_home}/include/python${Python_config_version_twodigit}
                                               -m ${Python_home}/lib/python${Python_config_version_twodigit}/config
                                               -q ${Qt_home}/bin/qmake
                                               -v <INSTALL_DIR>/share/sip/PyQt4
                                               -p <INSTALL_DIR>/plugins --verbose --confirm-license

  BUILD_IN_SOURCE 1
  DEPENDS Python Qt sip 
)

 LCGPackage_Add(
  pyqt5
  URL ${GenURL}/PyQt-gpl-${pyqt5_native_version}.tar.gz
  PATCH_COMMAND      ${CMAKE_COMMAND} -E copy ${CMAKE_BINARY_DIR}/qt5.conf qt.conf
  CONFIGURE_COMMAND  ${Python_cmd_pyqt} configure.py -b <INSTALL_DIR>/bin
                                               -d <INSTALL_DIR>/lib/python${Python_config_version_twodigit}/site-packages
                                               -l ${Python_home}/include/python${Python_config_version_twodigit}
                                               -m ${Python_home}/lib/python${Python_config_version_twodigit}/config
                                               -q ${Qt5_home}/bin/qmake
                                               -v <INSTALL_DIR>/share/sip/PyQt5
                                               --designer-plugindir <INSTALL_DIR>/plugins/designer
                                               --qml-plugindir <INSTALL_DIR>/plugins/PyQt5
                                               --sip ${sip_home}/bin/sip
                                               --sip-incdir ${sip_home}/include/python${Python_config_version_twodigit}
                                               --verbose --confirm-license

  BUILD_IN_SOURCE 1
  DEPENDS Python Qt5 sip
)

LCGPackage_Add(
  pygraphics
  DOWNLOAD_COMMAND <VOID>
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_SOURCE_DIR}/cmake/scripts/create_bundle.py <INSTALL_DIR> <BUNDLE_LIST>
          COMMAND ${CMAKE_SOURCE_DIR}/pyexternals/Python_postinstall.sh <INSTALL_DIR> 
  BUNDLE_PACKAGE 1
  DEPENDS pydot sip pyqt
)

#=================================================================================================================
#---pytools----------------------------------------------------------------------------------------------------
# This is only a placeholder where all the sub-packages will be installed
#=================================================================================================================
LCGPackage_set_home(pytools)

#---setuptools------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  setuptools
  URL ${GenURL}/setuptools-${setuptools_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---wheel------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  wheel
  URL ${GenURL}/wheel-${wheel_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---distribute---------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  distribute
  URL ${GenURL}/distribute-${distribute_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---lxml---------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  lxml
  URL ${GenURL}/lxml-${lxml_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python libxml2 libxslt
)

#---sqlalchemy---------------------------------------------------------------------------------------------------
LCGPackage_Add(
  sqlalchemy
  URL ${GenURL}/SQLAlchemy-${sqlalchemy_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---genshi------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  genshi
  URL ${GenURL}/Genshi-${genshi_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir} 
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---4suite------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  4suite
  URL ${GenURL}/4Suite-XML-${4suite_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---cx_oracle------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  cx_oracle
  URL ${GenURL}/cx_Oracle-${cx_oracle_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  ENVIRONMENT ORACLE_HOME=${oracle_home}
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python oracle
)

#---json------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  json
  URL ${GenURL}/simplejson-${json_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---mock------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  mock
  URL ${GenURL}/mock-${mock_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---multiprocessing------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  multiprocessing
  URL ${GenURL}/multiprocessing-${multiprocessing_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---mysql_python------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  mysql_python
  URL ${GenURL}/MySQL-python-${mysql_python_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools mysql
)

#---six------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  six
  URL ${GenURL}/six-${six_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
          #IF LCG_SOURCE_INSTALL THEN COMMAND $ENV{SHELL} -c "chmod -R 755 <INSTALL_DIR>/../share/sources/six" ENDIF
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---python_dateutil------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  python_dateutil
  URL ${GenURL}/python-dateutil-${python_dateutil_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools six
) 

#---pyzmq------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  pyzmq
  URL ${GenURL}/pyzmq-${pyzmq_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---pytz-------------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  pytz
  URL ${GenURL}/pytz-${pytz_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python 
)

LCGPackage_set_home(pyanalysis)

#---numpy---------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  numpy
  URL ${GenURL}/numpy-${numpy_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---pandas------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  pandas
  URL ${GenURL}/pandas-${pandas_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools pytz python_dateutil numpy
)


#---decorator------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  decorator
  URL ${GenURL}/decorator-${decorator_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---networkx------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  networkx
  URL ${GenURL}/networkx-${networkx_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools decorator
)
#---CouchDB------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  CouchDB
  URL ${GenURL}/CouchDB-${CouchDB_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---requests------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  requests
  URL ${GenURL}/requests-${requests_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---pyapigitlab------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  pyapigitlab
  URL ${GenURL}/pyapi-gitlab-${pyapigitlab_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools requests
)


#---pathos------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  pathos
  URL ${GenURL}/pathos-${pathos_native_version}.tgz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)


#---pygments------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  pygments
  URL ${GenURL}/Pygments-${pygments_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---mistune------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  mistune
  URL ${GenURL}/mistune-${mistune_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---prettytable------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  prettytable
  URL ${GenURL}/prettytable-${prettytable_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---pip------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  pip
  URL ${GenURL}/pip-${pip_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools wheel
)
#---PyYAML------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  PyYAML
  URL ${GenURL}/PyYAML-${PyYAML_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---subprocess32------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  subprocess32
  URL ${GenURL}/subprocess32-${subprocess32_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---MarkupSafe------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  MarkupSafe
  URL ${GenURL}/MarkupSafe-${MarkupSafe_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools 
)

#---Jinja2------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  Jinja2
  URL ${GenURL}/Jinja2-${Jinja2_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools MarkupSafe
)

#---jsonschema------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  jsonschema
  URL ${GenURL}/jsonschema-${jsonschema_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---certifi------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  certifi
  URL ${GenURL}/certifi-${certifi_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---backports------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  backports
  URL ${GenURL}/backports.ssl_match_hostname-${backports_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---tornado------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  tornado
  URL ${GenURL}/tornado-${tornado_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools certifi backports
)

#---terminado------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  terminado
  URL ${GenURL}/terminado-${terminado_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools tornado
)

#---ptyprocess------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  ptyprocess
  URL ${GenURL}/ptyprocess-${ptyprocess_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---messaging------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  messaging	
  URL ${GenURL}/python-messaging-master-${messaging_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---processing------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  processing
  URL ${GenURL}/processing-${processing_native_version}.zip
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---py4j------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  py4j
  URL ${GenURL}/py4j-${py4j_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---py------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  py
  URL ${GenURL}/py-${py_native_version}.zip
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND   <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools 
)

#---py2neo---------------------------------------------------------------------------------------------------
LCGPackage_Add(
  py2neo
  URL ${GenURL}/py2neo-${py2neo_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---pyxml------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  pyxml
  URL ${GenURL}/PyXML-${pyxml_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---strom------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  storm
  URL ${GenURL}/storm-${storm_native_version}.tar.bz2
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---pytest------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  pytest
  URL ${GenURL}/pytest-${pytest_native_version}.zip
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND  <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python py
)

#---logilab-common------------------------------------------------------------------------------------------------
LCGPackage_Add(
  logilabcommon
  URL ${GenURL}/logilab-common-${logilabcommon_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools six pytest
)

#---astroid------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  astroid
  URL ${GenURL}/astroid-${astroid_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python six logilabcommon
)

#---pylint------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  pylint
  URL ${GenURL}/pylint-${pylint_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python six astroid
)

#---nose------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  nose
  URL ${GenURL}/nose-${nose_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---coverage------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  coverage
  URL ${GenURL}/coverage-${coverage_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---stomppy------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  stomppy
  URL ${GenURL}/stomp.py-${stomppy_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---ipython------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  ipython
  DOWNLOAD_COMMAND <VOID>
  #URL ${GenURL}/ipython-${ipython_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python ${pip_home}/bin/pip install --prefix=<INSTALL_DIR>  ipython==${ipython_native_version}
  BUILD_IN_SOURCE 1
  DEPENDS Python pip setuptools ptyprocess
)

#---metakernel------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  metakernel
  DOWNLOAD_COMMAND <VOID>
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID> 
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python ${pip_home}/bin/pip install --prefix=<INSTALL_DIR>  metakernel==${metakernel_native_version}
  DEPENDS Python pip setuptools ipython six backports 
)

#---Jupyter------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  jupyter
  DOWNLOAD_COMMAND <VOID>
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID> 
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python ${pip_home}/bin/pip install --prefix=<INSTALL_DIR>  jupyter==${jupyter_native_version}
  DEPENDS Python pip setuptools metakernel pyzmq tornado pygments mistune MarkupSafe jsonschema Jinja2 
          certifi backports terminado
)

LCGPackage_Add(
  pytools
  DOWNLOAD_COMMAND <VOID>
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_SOURCE_DIR}/cmake/scripts/create_bundle.py <INSTALL_DIR> <BUNDLE_LIST>
          COMMAND ${CMAKE_SOURCE_DIR}/pyexternals/Python_postinstall.sh <INSTALL_DIR> 
  BUNDLE_PACKAGE 1
  DEPENDS lxml sqlalchemy genshi 4suite cx_oracle ipython json mock multiprocessing setuptools mysql_python processing py 
          py2neo pyxml storm pytest nose coverage stomppy pyzmq subprocess32 Jinja2 jsonschema terminado ptyprocess pandas 
          pygments mistune prettytable pip PyYAML pathos py4j wheel CouchDB networkx decorator pyapigitlab requests ${matplotlib_extra_deps}
)

#=================================================================================================================
#---pyanalysis----------------------------------------------------------------------------------------------------
# This is only a placeholder where all the sub-packages will be installed
#=================================================================================================================
#LCGPackage_set_home(pyanalysis)
#---pyparsing------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  pyparsing
  URL ${GenURL}/pyparsing-${pyparsing_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---numexpr------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  numexpr
  URL ${GenURL}/numexpr-${numexpr_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python numpy
)

#---scikitlearn------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  scikitlearn
  URL ${GenURL}/scikit-learn-${scikitlearn_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python numpy
)

#---scipy---------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  scipy
  URL ${GenURL}/scipy-${scipy_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  ENVIRONMENT BLAS=${blas_home}/lib/libBLAS.a 
              LAPACK=${lapack_home}/lib/libLAPACK.a
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools numpy blas lapack
)

#---sympy---------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  sympy
  URL ${GenURL}/sympy-${sympy_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python
)

#---matplotlib----------------------------------------------------------------------------------------------------
if(APPLE)
  set(matplotlib_extra_deps ${matplotlib_extra_deps} freetype pkg_config)
endif()
LCGPackage_Add(
  matplotlib
  URL ${GenURL}/matplotlib-${matplotlib_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools numpy nose pyparsing pytz mock distribute ${matplotlib_extra_deps}
)

#---pyminuit---------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  pyminuit
  URL ${GenURL}/pyminuit2-${pyminuit_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python ROOT
)

if(APPLE)
  set(library_path DYLD_LIBRARY_PATH)
else()
  set(library_path LD_LIBRARY_PATH)
endif()

#---rootpy---------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  rootpy
  URL ${GenURL}/rootpy-${rootpy_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  ENVIRONMENT PYTHONPATH=${ROOT_home}/lib ${library_path}=${ROOT_home}/lib:\$ENV{${library_path}}
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools ROOT 
)

#---rootpy---------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  root_numpy
  URL ${GenURL}/root_numpy-${root_numpy_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  ENVIRONMENT NOTMVA=1 PYTHONPATH=${ROOT_home}/lib ${library_path}=${ROOT_home}/lib:\$ENV{${library_path}}
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python ROOT numpy
)

#---Add the dependencies now----------------------------------------------------------------------------------------
LCGPackage_Add(
  pyanalysis
  DOWNLOAD_COMMAND <VOID>
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_SOURCE_DIR}/cmake/scripts/create_bundle.py <INSTALL_DIR> <BUNDLE_LIST>
          COMMAND ${CMAKE_SOURCE_DIR}/pyexternals/Python_postinstall.sh <INSTALL_DIR> 
  BUNDLE_PACKAGE 1
  DEPENDS matplotlib numpy pyparsing scipy sympy pyminuit pytz numexpr scikitlearn rootpy root_numpy 
)

#---libsvm------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  libsvm
  URL ${GenURL}/libsvm-${libsvm_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND make
        COMMAND make PYTHON_INCLUDEDIR=${Python_home}/include/python${Python_config_version_twodigit} 
                     -C python all LDFLAGS=\ -shared\ -L${Python_home}/lib\ -lpython${Python_config_version_twodigit}
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND ${CMAKE_COMMAND} -E make_directory <INSTALL_DIR>/bin
          COMMAND cp svm-train svm-predict svm-scale <INSTALL_DIR>/bin
          COMMAND ${CMAKE_COMMAND} -E make_directory <INSTALL_DIR>/include
          COMMAND ${CMAKE_COMMAND} -E copy svm.h <INSTALL_DIR>/include
          COMMAND ${CMAKE_COMMAND} -E make_directory <INSTALL_DIR>/src
          COMMAND ${CMAKE_COMMAND} -E copy svm.cpp <INSTALL_DIR>/src
          COMMAND ${CMAKE_COMMAND} -E make_directory <INSTALL_DIR>/python
          COMMAND cp python/svmc.so python/svm.py python/svm_test.py python/README <INSTALL_DIR>/python
          COMMAND cp -r README tools <INSTALL_DIR>

  BUILD_IN_SOURCE 1
  DEPENDS Python 
)

#---minuit------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  minuit
  URL ${GenURL}/Minuit2-${minuit_native_version}.tar.gz
  CONFIGURE_COMMAND ./configure --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python pyanalysis
)

#---pygsi-------------------------------------------------------------------------------------------------------
LCGPackage_Add(
  pygsi
  URL ${GenURL}/pygsi-${pygsi_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir}
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python setuptools
)

#---xroot_python------------------------------------------------------------------------------------------------
LCGPackage_Add(
  xrootd_python
  URL ${GenURL}/xrootd-python-${xrootd_python_native_version}.tar.gz
  CONFIGURE_COMMAND <VOID>
  ENVIRONMENT XRD_LIBDIR=${xrootd_home}/${LCG_LIBDIR_DEFAULT}
              XRD_INCDIR=${xrootd_home}/include/xrootd
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${MakeSitePackagesDir} 
          COMMAND python setup.py install --prefix <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  DEPENDS Python xrootd
)
